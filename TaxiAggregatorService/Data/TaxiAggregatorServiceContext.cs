﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TaxiAggregatorService.Models;

namespace TaxiAggregatorService.Data
{
    public class TaxiAggregatorServiceContext : DbContext
    {
        public TaxiAggregatorServiceContext (DbContextOptions<TaxiAggregatorServiceContext> options)
            : base(options)
        {
        }

        public DbSet<TaxiAggregatorService.Models.User> User { get; set; } = default!;
        public DbSet<TaxiAggregatorService.Models.Car> Car { get; set; } = default!;
        public DbSet<TaxiAggregatorService.Models.Aggregator> Aggregator { get; set; } = default!;
        public DbSet<TaxiAggregatorService.Models.Driver> Driver { get; set; } = default!;
        public DbSet<TaxiAggregatorService.Models.Passenger> Passenger { get; set; } = default!;
        public DbSet<TaxiAggregatorService.Models.Review> Review { get; set; } = default!;
        public DbSet<TaxiAggregatorService.Models.Transfer> Transfer { get; set; } = default!;
    }
}
