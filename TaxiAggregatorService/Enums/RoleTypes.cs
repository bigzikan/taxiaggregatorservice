﻿namespace TaxiAggregatorService.Enums
{
    public enum RoleTypes
    {
        AGGREGATOR,
        PASSENGER,
        DRIVER,
        ADMIN
    }
}
