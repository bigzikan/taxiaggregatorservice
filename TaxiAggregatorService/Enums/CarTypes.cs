﻿namespace TaxiAggregatorService.Enums
{
    public enum CarTypes
    {
        STANDARD,
        COMFORT,
        COMFORTPLUS,
        CHILDRENS
    }
}
