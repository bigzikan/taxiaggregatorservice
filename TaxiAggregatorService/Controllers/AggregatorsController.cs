﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TaxiAggregatorService.Data;
using TaxiAggregatorService.Models;

namespace TaxiAggregatorService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AggregatorsController : ControllerBase
    {
        private readonly TaxiAggregatorServiceContext _context;

        public AggregatorsController(TaxiAggregatorServiceContext context)
        {
            _context = context;
        }

        // GET: api/Aggregators/getDriversByAdress
        [Route("/getDriversByAdress")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Driver>>> GetDriversByAdress(string address, CancellationToken cancellationToken)
        {
            return await Task.Run(() => { return new List<Driver> (); });
        }

        // GET: api/Aggregators/getDriversByAdress
        [Route("/setOrder")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Transfer>>> SetOrder(Driver driver, Passenger passenger, CancellationToken cancellationToken)
        {

            return await Task.Run(() => { return new List<Transfer>(); });
        }

        // GET: api/Aggregators/getDriversByAdress
        [Route("/cancelOrder")]
        [HttpGet]
        public async Task CancelOrder(Transfer transfer, CancellationToken cancellationToken)
        {
            return;            
        }

        // GET: api/Aggregators
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Aggregator>>> GetAggregator()
        {
          if (_context.Aggregator == null)
          {
              return NotFound();
          }
            return await _context.Aggregator.ToListAsync();
        }

        // GET: api/Aggregators/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Aggregator>> GetAggregator(long id)
        {
          if (_context.Aggregator == null)
          {
              return NotFound();
          }
            var aggregator = await _context.Aggregator.FindAsync(id);

            if (aggregator == null)
            {
                return NotFound();
            }

            return aggregator;
        }

        // PUT: api/Aggregators/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAggregator(long id, Aggregator aggregator)
        {
            if (id != aggregator.Id)
            {
                return BadRequest();
            }

            _context.Entry(aggregator).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AggregatorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Aggregators
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Aggregator>> PostAggregator(Aggregator aggregator)
        {
          if (_context.Aggregator == null)
          {
              return Problem("Entity set 'TaxiAggregatorServiceContext.Aggregator'  is null.");
          }
            _context.Aggregator.Add(aggregator);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAggregator", new { id = aggregator.Id }, aggregator);
        }

        // DELETE: api/Aggregators/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAggregator(long id)
        {
            if (_context.Aggregator == null)
            {
                return NotFound();
            }
            var aggregator = await _context.Aggregator.FindAsync(id);
            if (aggregator == null)
            {
                return NotFound();
            }

            _context.Aggregator.Remove(aggregator);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool AggregatorExists(long id)
        {
            return (_context.Aggregator?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
