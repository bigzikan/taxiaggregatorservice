﻿using SQLite;
using System.ComponentModel.DataAnnotations;

namespace TaxiAggregatorService.Models
{
    public class Aggregator
    {
        public long Id { get; set; }

        public string name { get; set; }

        public string email { get; set; }

        public string password { get; set; }

        public List<Passenger> passengers { get; set; } // Список пасажиров использующих агрегатор

        public List<Car> cars { get; set; }  // Список машин у этого агрегатора

        public List<Transfer> transfers { get; set; }
    }
}
