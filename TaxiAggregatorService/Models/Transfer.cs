﻿using SQLite;
using System.ComponentModel.DataAnnotations;

namespace TaxiAggregatorService.Models
{
    public class Transfer
    {
        [PrimaryKey]
        public long id { get; set; }

        public Car car { get; set; }

        public Passenger passenger { get; set; }

        public Driver driver { get; set; }

        public string addressExtraction { get; set; }// Адрес отправления

        public string addressDeparture { get; set; }// Адрес прибытия

        public DateTime begin_date_arrival { get; set; }// Начало диапазона времени подачи автомобиля

        public DateTime end_date_arrival { get; set; }// Окончание диапазона времени подачи автомобиля

        public DateTime begin_date_departure { get; set; }// Начало диапазона времени отправления

        public DateTime end_date_departure { get; set; }// Окончание диапазона времени отправления

        public DateTime begin_date_reservation { get; set; }// Начало дипазона времени бронирования

        public DateTime end_date_reservation { get; set; }// Окончание дипазона времени бронирования

        public List<string> reviewsTransfer { get { return reviewsTransfer; } }// Отзывы

        public float price { get; set; }

        public float distance { get; set; }
    }
}
