﻿using SQLite;
using System.ComponentModel.DataAnnotations;

namespace TaxiAggregatorService.Models
{
    public class User
    {
        [PrimaryKey]
        public long id { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public string phone { get; set; }

        public string email { get; set; }

        public bool gender { get; set; }
    }
}
