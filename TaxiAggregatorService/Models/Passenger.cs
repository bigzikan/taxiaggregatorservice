﻿using SQLite;
using System.ComponentModel.DataAnnotations;
using TaxiAggregatorService.Enums;

namespace TaxiAggregatorService.Models
{
    public class Passenger
    {
        public long Id { get; set; }
        public float weight { get; set; } // Вес

        public float height { get; set; } // Рост

        public List<Transfer> transfers { get; set; }

        public List<Review> reviews { get { return reviews; } }

        public HashSet<RoleTypes> roles { get { return roles; } }

        public User user;
    }
}
