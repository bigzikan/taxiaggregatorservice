﻿using SQLite;
using System.ComponentModel.DataAnnotations;
using TaxiAggregatorService.Enums;

namespace TaxiAggregatorService.Models
{
    public class Car
    {
        public long Id { get; set; }

        public Driver driver { get; set; }

        public string title { get; set; }

        public int capacity { get; set; } // Вместимость пассажиров    

        public float maxBaggageWeight { get; set; } // Максимальная масса багажа    

        public float mileage { get; set; } // Пробег        

        public DateTime dateOfRelease { get; set; } // Дата производства

        public string number { get; set; } // Номер машины

        public string vinNumber { get; set; }  // VIN номер

        public CarTypes carType { get; set; } // Класс авто
    }
}
