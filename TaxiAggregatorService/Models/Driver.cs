﻿using SQLite;
using System.ComponentModel.DataAnnotations;
using TaxiAggregatorService.Enums;

namespace TaxiAggregatorService.Models
{
    public class Driver
    {
        [Key]
        public long driver_id { get; set; }
        public float drivingExperience { get; set; } // Стаж вождения

        public float rating { get; set; }// Рейтинг

        public List<string> reviewsDriver { get { return reviewsDriver; } }// Отзывы

        public HashSet<RoleTypes> roles { get { return roles; } }
    }
}
