﻿using SQLite;
using System.ComponentModel.DataAnnotations;

namespace TaxiAggregatorService.Models
{
    public class Review
    {
        public long Id { get; set; }

        public string review { get; set; }

        public DateTime reviewDateTime { get; set; }
    }
}
